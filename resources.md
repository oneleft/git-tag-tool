# Resources

* http://stackoverflow.com/questions/7740683/set-enviroment-variable-with-having-space-linux
* http://stackoverflow.com/questions/4404172/tag-older-commit-in-git
* http://stackoverflow.com/questions/5343068/is-there-a-way-to-skip-password-typing-when-using-https-on-github
* http://stackoverflow.com/questions/5480258/how-to-delete-a-remote-tag
* https://confluence.atlassian.com/bitbucketserver/permanently-authenticating-with-git-repositories-776639846.html
* http://stackoverflow.com/questions/17203122/bash-if-else-statement-in-one-line
* https://confluence.atlassian.com/bitbucket/how-do-i-remove-or-delete-a-tag-from-a-git-repo-282175551.html
* http://stackoverflow.com/questions/2981878/checking-for-environment-variables